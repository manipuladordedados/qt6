/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#pragma once

#include <QtCore/QObject>

namespace emilua_qt {

class ByteSpanResource : public QObject
{
    Q_OBJECT
public:
    explicit ByteSpanResource(QObject* parent = nullptr);

    std::shared_ptr<unsigned char[]> data;
};

} // namespace emilua_qt
