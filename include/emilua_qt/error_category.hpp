/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#pragma once

#include <system_error>

namespace emilua_qt {

enum class errc {
    failed_to_load_qml = 1,
    wrong_signature,
    too_many_arguments,
};

const std::error_category& category();

inline std::error_code make_error_code(errc e)
{
    return std::error_code{static_cast<int>(e), category()};
}

} // namespace emilua_qt

template<>
struct std::is_error_code_enum<emilua_qt::errc>: std::true_type {};
