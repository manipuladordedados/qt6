local qt = require 'qt6'

local qml = qt.load_qml(byte_span.append([[
    import QtQml

    QtObject {
        id: attributes
        property string my_property: "wow"
    }
]]))

print(qml.property.my_property)
print(qml.object.my_property)
print(qml.property.attributes.my_property)

qml.object.my_property = "bar"

print(qml.property.my_property)
print(qml.object.my_property)
print(qml.property.attributes.my_property)
