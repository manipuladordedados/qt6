local qt = require 'qt6'

local qml = qt.load_qml(byte_span.append([[
    import QtQml

    QtObject {
        function foobar(a: int, b: int): int {
            return a + b
        }
    }
]]))

print(qml.object('foobar(int,int)', 1, 2))
