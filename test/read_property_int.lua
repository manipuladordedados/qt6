local qt = require 'qt6'

local qml = qt.load_qml(byte_span.append([[
    import QtQml

    QtObject {
        id: attributes
        property int my_property: 1
    }
]]))

print(qml.property.my_property)
print(qml.object.my_property)
print(qml.property.attributes.my_property)
