/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua/plugin.hpp>
#include <emilua_qt/qt_handle.hpp>
#include <emilua_qt/service.hpp>

namespace emilua_qt {

class qt_plugin : emilua::plugin
{
public:
    std::error_code init_ioctx_services(
        std::shared_lock<std::shared_mutex>&,
        boost::asio::io_context& ioctx) noexcept override;
    std::error_code init_lua_module(
        std::shared_lock<std::shared_mutex>&,
        emilua::vm_context& vm_ctx, lua_State* L) override;

private:
    static std::weak_ptr<qt_handle> handle;
    static std::mutex handle_mtx;
};

std::weak_ptr<qt_handle> qt_plugin::handle;
std::mutex qt_plugin::handle_mtx;

std::error_code qt_plugin::init_ioctx_services(
    std::shared_lock<std::shared_mutex>&,
    boost::asio::io_context& ioctx) noexcept
{
    std::shared_ptr<qt_handle> handle;

    {
        const std::lock_guard<std::mutex> lock{handle_mtx};
        handle = qt_plugin::handle.lock();
        if (!handle) try {
            handle = std::make_shared<qt_handle>();
            qt_plugin::handle = handle;
        } catch (const std::system_error& e) {
            return e.code();
        }
    }

    try {
        make_service<service>(ioctx, std::move(handle));
    } catch (const boost::asio::service_already_exists&) {}
    return {};
}

std::error_code qt_plugin::init_lua_module(
    std::shared_lock<std::shared_mutex>&, emilua::vm_context& /*vm_ctx*/,
    lua_State* L)
{
    std::shared_ptr<qt_handle> handle;

    {
        const std::lock_guard<std::mutex> lock{handle_mtx};
        handle = qt_plugin::handle.lock();
        assert(handle);
    }

    return handle->init_lua_module(L);
}

} // namespace emilua_qt

extern "C" BOOST_SYMBOL_EXPORT emilua_qt::qt_plugin EMILUA_PLUGIN_SYMBOL;
emilua_qt::qt_plugin EMILUA_PLUGIN_SYMBOL;
