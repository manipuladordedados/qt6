/* Copyright (c) 2023, 2024 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

EMILUA_GPERF_DECLS_BEGIN(includes)
#include <QtCore/QResource>

#include <QtQml/QQmlComponent>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>

#include <QtGui/QGuiApplication>

#include <emilua/async_base.hpp>
#include <emilua/filesystem.hpp>
#include <emilua/byte_span.hpp>
#include <emilua/core.hpp>

#include <emilua_qt/bytespanresource.hpp>
#include <emilua_qt/error_category.hpp>
#include <emilua_qt/workqueue.hpp>
#include <emilua_qt/observer.hpp>
EMILUA_GPERF_DECLS_END(includes)

namespace emilua_qt {

EMILUA_GPERF_DECLS_BEGIN(qt_loop)
EMILUA_GPERF_NAMESPACE(emilua_qt)
namespace hana = boost::hana;
namespace asio = boost::asio;
namespace fs = std::filesystem;

using emilua::vm_context;
using emilua::get_vm_context;
using emilua::tostringview;
using emilua::setmetatable;
using emilua::rawgetp;
using emilua::push;
using emilua::finalizer;
using emilua::byte_span_handle;
using emilua::byte_span_mt_key;
using emilua::filesystem_path_mt_key;

static char qml_context_mt_key;
static char qml_context_property_mt_key;
static char qobject_mt_key;

static int argc = 1;
static const char* argv[] = { "emilua" };
static QGuiApplication* app;
static WorkQueue* workQueue;
EMILUA_GPERF_DECLS_END(qt_loop)

static void on_index_resume_fiber(
    vm_context& vm_ctx, lua_State* fiber,
    const QVariant& var, const std::shared_ptr<QObject>& strong_ref
) {
    static constexpr auto opt_args = vm_context::options::arguments;

    if (!var.isValid()) {
        std::error_code ec{emilua::errc::bad_index};
        vm_ctx.fiber_resume(
            fiber,
            hana::make_set(
                vm_context::options::skip_clear_interrupter,
                hana::make_pair(
                    opt_args, hana::make_tuple(ec))));
        return;
    }

    auto push_object = [&var,&strong_ref](lua_State* L) {
        switch (var.typeId()) {
        case QMetaType::Bool:
            lua_pushboolean(L, var.toBool());
            break;
        case QMetaType::Double:
            lua_pushnumber(L, var.toDouble());
            break;
        case QMetaType::Int:
            lua_pushinteger(L, var.toInt());
            break;
        case QMetaType::QString: {
            auto s = var.toString().toUtf8();
            lua_pushlstring(L, s.data(), s.size());
            break;
        }
        case QMetaType::QUrl: {
            auto s = var.toUrl().toEncoded();
            lua_pushlstring(L, s.data(), s.size());
            break;
        }
        default:
            if (auto object = var.value<QObject*>() ; object) {
                auto ret = static_cast<std::shared_ptr<QObject>*>(
                    lua_newuserdata(L, sizeof(std::shared_ptr<QObject>))
                );
                rawgetp(L, LUA_REGISTRYINDEX, &qobject_mt_key);
                setmetatable(L, -2);
                new (ret) std::shared_ptr<QObject>{strong_ref, object};
            } else {
                lua_pushnil(L);
            }
            break;
        }
    };
    vm_ctx.fiber_resume(
        fiber,
        hana::make_set(
            vm_context::options::skip_clear_interrupter,
            hana::make_pair(
                opt_args,
                hana::make_tuple(std::nullopt, push_object))));
}

extern "C" BOOST_SYMBOL_EXPORT void init_loop() noexcept
{
    QCoreApplication::setAttribute(Qt::AA_PluginApplication, true);

    app = new QGuiApplication{argc, (char**)argv};
    app->setQuitOnLastWindowClosed(false);

    workQueue = new WorkQueue{app};
}

extern "C" BOOST_SYMBOL_EXPORT void loop_start() noexcept
{
    app->exec();

    // some QPA plugins are buggy and will randomly crash if we free resources
#if 0
    delete app;
#endif // 0
}

extern "C" BOOST_SYMBOL_EXPORT void loop_end() noexcept
{
    QMetaObject::invokeMethod(app, "exit", Qt::QueuedConnection);
}

static int load_qml(lua_State* L)
{
    static constexpr auto opt_args = vm_context::options::arguments;

    lua_settop(L, 1);

    std::string data;
    bool is_url;

    switch (lua_type(L, 1)) {
    case LUA_TUSERDATA: {
        auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
        if (!bs || !lua_getmetatable(L, 1)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }
        rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
        if (!lua_rawequal(L, -1, -2)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }
        data = static_cast<std::string_view>(*bs);
        is_url = false;
        break;
    }
    case LUA_TSTRING:
        data = tostringview(L, 1);
        is_url = true;
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,data,is_url
    ]() {
        auto engine = new QQmlEngine{app};
        auto component = new QQmlComponent{engine, app};
        QObject::connect(
            component, &QQmlComponent::statusChanged,
            [
                work_guard,vm_ctx,current_fiber,engine,component
            ](QQmlComponent::Status status) {
                if (status == QQmlComponent::Error) {
                    auto errors = [errors=component->errors()](lua_State* L) {
                        push(L, errc::failed_to_load_qml);

                        lua_pushliteral(L, "errors");
                        lua_newtable(L);
                        for (int i = 0 ; i != errors.size() ; ++i) {
                            auto e = errors[i].toString().toUtf8();
                            lua_pushlstring(L, e.data(), e.size());
                            lua_rawseti(L, -2, i + 1);
                        }
                        lua_rawset(L, -3);
                    };
                    vm_ctx->strand().post([vm_ctx,current_fiber,errors]() {
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(errors))));
                    }, std::allocator<void>{});
                    component->deleteLater();
                    engine->deleteLater();
                    return;
                }

                if (status != QQmlComponent::Ready)
                    return;

                QObject* rootObject = component->create();
                rootObject->setParent(engine);
                component->deleteLater();

                std::shared_ptr<QQmlEngine> engine2{
                    engine, [](QQmlEngine* o) { o->deleteLater(); }};
                std::shared_ptr<QQmlContext> rootContext{
                    std::move(engine2), qmlContext(rootObject)};

                vm_ctx->strand().post([vm_ctx,current_fiber,rootContext]() {
                    auto push_qml_context = [rootContext](lua_State* L) {
                        auto ret = static_cast<std::shared_ptr<QQmlContext>*>(
                            lua_newuserdata(
                                L, sizeof(std::shared_ptr<QQmlContext>))
                        );
                        rawgetp(L, LUA_REGISTRYINDEX, &qml_context_mt_key);
                        setmetatable(L, -2);
                        new (ret) std::shared_ptr<QQmlContext>{rootContext};
                    };
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter,
                            hana::make_pair(
                                opt_args,
                                hana::make_tuple(
                                    std::nullopt, push_qml_context))));
                }, std::allocator<void>{});
            });
        if (is_url) {
            component->loadUrl(
                QUrl::fromEncoded(QByteArray::fromStdString(data)));
        } else {
            component->setData(QByteArray::fromStdString(data), QUrl{});
        }
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int register_resource(lua_State* L)
{
    lua_settop(L, 2);

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    if (lua_type(L, 1) != LUA_TUSERDATA) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    std::shared_ptr<unsigned char[]> data;
    QString file_name;

    auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
    if (!bs || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
    if (lua_rawequal(L, -1, -2)) {
        data = bs->data;
    } else {
        rawgetp(L, LUA_REGISTRYINDEX, &filesystem_path_mt_key);
        if (!lua_rawequal(L, -1, -3)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }

        auto path = static_cast<fs::path*>(lua_touserdata(L, 1))->u16string();
        file_name = QString::fromUtf16(path.data(), path.size());
    }

    QString map_root;
    switch (lua_type(L, 2)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    case LUA_TNIL:
        break;
    case LUA_TSTRING: {
        auto s = tostringview(L, 2);
        map_root = QString::fromUtf8(s.data(), s.size());
    }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,data,file_name,
        map_root
    ]() {
        bool ret;
        if (data) {
            ret = QResource::registerResource(data.get(), map_root);
            if (ret) {
                auto ref = new ByteSpanResource{app};
                ref->data = data;
            }
        } else {
            ret = QResource::registerResource(file_name, map_root);
        }

        static constexpr auto opt_args = vm_context::options::arguments;

        vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter,
                    hana::make_pair(
                        opt_args, hana::make_tuple(ret))));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int unregister_resource(lua_State* L)
{
    lua_settop(L, 2);

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    if (lua_type(L, 1) != LUA_TUSERDATA) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    std::shared_ptr<unsigned char[]> data;
    QString file_name;

    auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
    if (!bs || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
    if (lua_rawequal(L, -1, -2)) {
        data = bs->data;
    } else {
        rawgetp(L, LUA_REGISTRYINDEX, &filesystem_path_mt_key);
        if (!lua_rawequal(L, -1, -3)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }

        auto path = static_cast<fs::path*>(lua_touserdata(L, 1))->u16string();
        file_name = QString::fromUtf16(path.data(), path.size());
    }

    QString map_root;
    switch (lua_type(L, 2)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    case LUA_TNIL:
        break;
    case LUA_TSTRING: {
        auto s = tostringview(L, 2);
        map_root = QString::fromUtf8(s.data(), s.size());
    }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,data,file_name,
        map_root
    ]() {
        bool ret;
        if (data) {
            ret = QResource::unregisterResource(data.get(), map_root);
            if (ret) {
                for (auto& o : app->children()) {
                    if (auto bsr = qobject_cast<ByteSpanResource*>(o) ; bsr) {
                        if (bsr->data == data) {
                            bsr->deleteLater();
                            break;
                        }
                    }
                }
            }
        } else {
            ret = QResource::unregisterResource(file_name, map_root);
        }

        static constexpr auto opt_args = vm_context::options::arguments;

        vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter,
                    hana::make_pair(
                        opt_args, hana::make_tuple(ret))));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qobject_mt_index(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& object = *static_cast<std::shared_ptr<QObject>*>(
        lua_touserdata(L, 1));
    std::string key{tostringview(L, 2)};

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,key
    ]() {
        auto var = object->property(key.c_str());
        vm_ctx->strand().post([vm_ctx,current_fiber,var,object]() {
            on_index_resume_fiber(*vm_ctx, current_fiber, var, object);
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qobject_mt_newindex(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& object = *static_cast<std::shared_ptr<QObject>*>(
        lua_touserdata(L, 1));
    auto key = tostringview(L, 2);

    QVariant value;
    switch (lua_type(L, 3)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    case LUA_TBOOLEAN:
        value.setValue<bool>(lua_toboolean(L, 3));
        break;
    case LUA_TNUMBER:
        value.setValue(lua_tonumber(L, 3));
        break;
    case LUA_TSTRING:
        value.setValue<QString>(lua_tostring(L, 3));
        break;
    case LUA_TNIL: {
        auto work = [
            work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,
            key=std::string{key}
        ]() {
            for (auto& o : object->children()) {
                if (auto observer = qobject_cast<Observer*>(o) ; observer) {
                    if (observer->subscribedSignal == key)
                        observer->deleteLater();
                }
            }

            if (object->property(key.c_str()).isValid()) {
                object->setProperty(key.c_str(), QVariant{});
            }

            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
        };
        app->postEvent(workQueue, new WorkUnitEvent(work));
        return lua_yield(L, 0);
    }
    case LUA_TFUNCTION: {
        // QObject::metaObject() is not officially thread-safe. However we deem
        // it as so here. The metaobject cannot change during the object
        // lifetime. The metaobject is just a pointer to a static variable. It
        // makes no sense to assume otherwise. QObject::staticMetaObject exists,
        // but it's not usable here.
        auto metaObject = object->metaObject();
        auto signalIdx = metaObject->indexOfSignal(
            QMetaObject::normalizedSignature(key.data()));
        if (signalIdx == -1) {
            push(L, emilua::errc::bad_index, "index", 2);
            return lua_error(L);
        }
        auto signal = metaObject->method(signalIdx);

        lua_pushvalue(L, 3);
        int function = luaL_ref(L, LUA_REGISTRYINDEX);

        auto observer = new Observer{vm_ctx, function};
        observer->subscribedSignal = key;
        observer->moveToThread(app->thread());

        auto work = [
            work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,
            observer,key=std::string{key},signal
        ]() {
            for (auto& o : object->children()) {
                if (auto observer = qobject_cast<Observer*>(o) ; observer) {
                    if (observer->subscribedSignal == key)
                        observer->deleteLater();
                }
            }

            observer->setParent(object.get());

            auto metaObject = observer->metaObject();
            QMetaMethod genericSlot;

            for (int i = 0 ; i != metaObject->methodCount() ; ++i) {
                auto slot = metaObject->method(i);
                if (slot.methodType() != QMetaMethod::Slot ||
                    slot.name() != "map") {
                    continue;
                }

                if (slot.parameterCount() == 0) {
                    genericSlot = slot;
                }

                if (
                    (
                        signal.parameterCount() > 0 &&
                        slot.parameterCount() == 0
                    ) ||
                    !QMetaObject::checkConnectArgs(signal, slot)
                ) {
                    continue;
                }

                QObject::connect(object.get(), signal, observer, slot);
                vm_ctx->strand().post([vm_ctx,current_fiber]() {
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter));
                }, std::allocator<void>{});
                return;
            }

            QObject::connect(object.get(), signal, observer, genericSlot);
            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
        };
        app->postEvent(workQueue, new WorkUnitEvent(work));
        return lua_yield(L, 0);
    }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,
        key=std::string{key},value
    ]() {
        object->setProperty(key.c_str(), value);

        vm_ctx->strand().post([vm_ctx,current_fiber]() {
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qobject_mt_call(lua_State* L)
{
    static constexpr auto opt_args = vm_context::options::arguments;

    int nargs = lua_gettop(L);
    if (nargs < 2 || lua_type(L, 2) != LUA_TSTRING) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    if (nargs > 2 + 10) {
        push(L, errc::too_many_arguments);
        return lua_error(L);
    }

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& object = *static_cast<std::shared_ptr<QObject>*>(
        lua_touserdata(L, 1));
    std::string method{tostringview(L, 2)};

    QVariantList arguments;
    for (int i = 3 ; i <= nargs ; ++i) {
        switch (lua_type(L, i)) {
        case LUA_TNIL:
            arguments.emplace_back();
            arguments.back().setValue<std::nullptr_t>(nullptr);
            break;
        case LUA_TBOOLEAN:
            arguments.emplace_back(static_cast<bool>(lua_toboolean(L, i)));
            break;
        case LUA_TNUMBER:
            arguments.emplace_back(lua_tonumber(L, i));
            break;
        case LUA_TSTRING: {
            auto s = tostringview(L, i);
            arguments.emplace_back(QString::fromUtf8(s.data(), s.size()));
            break;
        }
        default:
            push(L, std::errc::invalid_argument, "arg", i);
            return lua_error(L);
        }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,method,
        arguments
    ]() {
        auto metaObject = object->metaObject();
        int indexOfMethod = metaObject->indexOfMethod(
            QMetaObject::normalizedSignature(method.c_str()));
        if (indexOfMethod == -1) {
            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                std::error_code ec{emilua::errc::bad_index};
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter,
                        hana::make_pair(opt_args, hana::make_tuple(ec))));
            }, std::allocator<void>{});
            return;
        }
        auto metaMethod = metaObject->method(indexOfMethod);
        if (metaMethod.parameterCount() != arguments.size()) {
            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                std::error_code ec{errc::wrong_signature};
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter,
                        hana::make_pair(opt_args, hana::make_tuple(ec))));
            }, std::allocator<void>{});
            return;
        }

        std::array<std::decay_t<decltype(Q_ARG(int, 0))>, 10> arguments2;
        assert(arguments2.size() >= arguments.size());

        std::array<bool, 10> boolBuffer;
        std::array<double, 10> doubleBuffer;
        std::array<int, 10> intBuffer;
        std::array<QString, 10> stringBuffer;
        std::array<QUrl, 10> urlBuffer;

        for (int i = 0 ; i != arguments.size() ; ++i) {
            switch (arguments[i].typeId()) {
            case QMetaType::Nullptr:
                if (metaMethod.parameterType(i) != QMetaType::QVariant) {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                arguments2[i] = Q_ARG(QVariant, arguments[i]);
                break;
            case QMetaType::Bool:
                switch (metaMethod.parameterType(i)) {
                case QMetaType::Bool:
                    boolBuffer[i] = arguments[i].toBool();
                    arguments2[i] = Q_ARG(bool, boolBuffer[i]);
                    break;
                case QMetaType::QVariant:
                    arguments2[i] = Q_ARG(QVariant, arguments[i]);
                    break;
                default: {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                }
                break;
            case QMetaType::Double:
                switch (metaMethod.parameterType(i)) {
                case QMetaType::Double:
                    doubleBuffer[i] = arguments[i].toDouble();
                    arguments2[i] = Q_ARG(double, doubleBuffer[i]);
                    break;
                case QMetaType::Int:
                    intBuffer[i] = arguments[i].toDouble();
                    arguments2[i] = Q_ARG(int, intBuffer[i]);
                    break;
                case QMetaType::QVariant:
                    arguments2[i] = Q_ARG(QVariant, arguments[i]);
                    break;
                default: {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                }
                break;
            case QMetaType::QString:
                switch (metaMethod.parameterType(i)) {
                case QMetaType::QString:
                    stringBuffer[i] = arguments[i].toString();
                    arguments2[i] = Q_ARG(QString, stringBuffer[i]);
                    break;
                case QMetaType::QUrl:
                    urlBuffer[i] = arguments[i].toString();
                    arguments2[i] = Q_ARG(QUrl, urlBuffer[i]);
                    break;
                case QMetaType::QVariant:
                    arguments2[i] = Q_ARG(QVariant, arguments[i]);
                    break;
                default: {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                }
                break;
            default:
                qFatal("internal error... shouldn't have invalid type stored "
                       "here");
                std::abort();
                break;
            }
        }

        auto dispatchWithReturn = [&](auto type) {
            typename decltype(type)::type retBuffer;
            switch (arguments.size()) {
            case 0:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer));
                break;
            case 1:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0]);
                break;
            case 2:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1]);
                break;
            case 3:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2]);
                break;
            case 4:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3]);
                break;
            case 5:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4]);
                break;
            case 6:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5]);
                break;
            case 7:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6]);
                break;
            case 8:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7]);
                break;
            case 9:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8]);
                break;
            case 10:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8], arguments2[9]);
                break;
            default:
                vm_ctx->strand().post([vm_ctx,current_fiber]() {
                    std::error_code ec{errc::too_many_arguments};
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter,
                            hana::make_pair(opt_args, hana::make_tuple(ec))));
                }, std::allocator<void>{});
                return;
            }

            QVariant ret = retBuffer;
            vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
                auto push_ret = [&ret](lua_State* L) {
                    if (!ret.isValid()) {
                        lua_pushnil(L);
                        return;
                    }

                    switch (ret.typeId()) {
                    case QMetaType::Nullptr:
                        lua_pushnil(L);
                        break;
                    case QMetaType::Bool:
                        lua_pushboolean(L, ret.toBool());
                        break;
                    case QMetaType::Double:
                        lua_pushnumber(L, ret.toDouble());
                        break;
                    case QMetaType::Int:
                        lua_pushinteger(L, ret.toInt());
                        break;
                    case QMetaType::QString: {
                        auto s = ret.toString().toUtf8();
                        lua_pushlstring(L, s.data(), s.size());
                        break;
                    }
                    case QMetaType::QUrl: {
                        auto s = ret.toUrl().toEncoded();
                        lua_pushlstring(L, s.data(), s.size());
                        break;
                    }
                    default:
                        lua_pushnil(L);
                    }
                };
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter,
                        hana::make_pair(
                            opt_args,
                            hana::make_tuple(std::nullopt, push_ret))));
            }, std::allocator<void>{});
        };

        switch (metaMethod.returnType()) {
        default:
        case QMetaType::Void:
            switch (arguments.size()) {
            case 0:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection);
                break;
            case 1:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0]);
                break;
            case 2:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1]);
                break;
            case 3:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2]);
                break;
            case 4:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3]);
                break;
            case 5:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4]);
                break;
            case 6:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5]);
                break;
            case 7:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6]);
                break;
            case 8:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7]);
                break;
            case 9:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8]);
                break;
            case 10:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8], arguments2[9]);
                break;
            default:
                vm_ctx->strand().post([vm_ctx,current_fiber]() {
                    std::error_code ec{errc::too_many_arguments};
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter,
                            hana::make_pair(opt_args, hana::make_tuple(ec))));
                }, std::allocator<void>{});
                return;
            }

            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
            return;
        case QMetaType::QVariant:
            return dispatchWithReturn(hana::type_c<QVariant>);
        case QMetaType::Bool:
            return dispatchWithReturn(hana::type_c<bool>);
        case QMetaType::Double:
            return dispatchWithReturn(hana::type_c<double>);
        case QMetaType::Int:
            return dispatchWithReturn(hana::type_c<int>);
        case QMetaType::QString:
            return dispatchWithReturn(hana::type_c<QString>);
        case QMetaType::QUrl:
            return dispatchWithReturn(hana::type_c<QUrl>);
        }
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qml_context_mt_index(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& context = *static_cast<std::shared_ptr<QQmlContext>*>(
        lua_touserdata(L, 1));
    auto key = tostringview(L, 2);

    return EMILUA_GPERF_BEGIN(key)
        EMILUA_GPERF_PARAM(
            int (*action)(
                lua_State*, std::shared_ptr<vm_context>&, lua_State*,
                std::shared_ptr<QQmlContext>&))
        EMILUA_GPERF_DEFAULT_VALUE(
            [](lua_State* L, std::shared_ptr<vm_context>&, lua_State*,
               std::shared_ptr<QQmlContext>&) {
                push(L, emilua::errc::bad_index, "index", 2);
                return lua_error(L);
            }
        )
        EMILUA_GPERF_PAIR(
            "property",
            [](lua_State* L, std::shared_ptr<vm_context>&,
               lua_State*, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;

                auto ret = static_cast<std::shared_ptr<QQmlContext>*>(
                    lua_newuserdata(L, sizeof(std::shared_ptr<QQmlContext>)));
                rawgetp(L, LUA_REGISTRYINDEX, &qml_context_property_mt_key);
                setmetatable(L, -2);
                new (ret) std::shared_ptr<QQmlContext>{context};
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "object",
            [](lua_State* L, std::shared_ptr<vm_context>& vm_ctx,
               lua_State* fiber, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;
                static constexpr auto opt_args = vm_context::options::arguments;

                auto work = [
                    work_guard=vm_ctx->work_guard(),vm_ctx,fiber,context
                ]() {
                    std::shared_ptr<QObject> object{
                        context, context->contextObject()};
                    vm_ctx->strand().post([vm_ctx,fiber,object]() {
                        auto push_object = [&object](lua_State* L) {
                            auto ret = static_cast<std::shared_ptr<QObject>*>(
                                lua_newuserdata(
                                    L, sizeof(std::shared_ptr<QObject>)));
                            rawgetp(L, LUA_REGISTRYINDEX, &qobject_mt_key);
                            setmetatable(L, -2);
                            new (ret) std::shared_ptr<QObject>{object};
                            return 1;
                        };
                        vm_ctx->fiber_resume(
                            fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(push_object))));
                    }, std::allocator<void>{});
                };
                app->postEvent(workQueue, new WorkUnitEvent(work));
                return lua_yield(L, 0);
            })
        EMILUA_GPERF_PAIR(
            "engine",
            [](lua_State* L, std::shared_ptr<vm_context>& vm_ctx,
               lua_State* fiber, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;
                static constexpr auto opt_args = vm_context::options::arguments;

                auto work = [
                    work_guard=vm_ctx->work_guard(),vm_ctx,fiber,context
                ]() {
                    std::shared_ptr<QObject> object{context, context->engine()};
                    vm_ctx->strand().post([vm_ctx,fiber,object]() {
                        auto push_object = [&object](lua_State* L) {
                            auto ret = static_cast<std::shared_ptr<QObject>*>(
                                lua_newuserdata(
                                    L, sizeof(std::shared_ptr<QObject>)));
                            rawgetp(L, LUA_REGISTRYINDEX, &qobject_mt_key);
                            setmetatable(L, -2);
                            new (ret) std::shared_ptr<QObject>{object};
                            return 1;
                        };
                        vm_ctx->fiber_resume(
                            fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(push_object))));
                    }, std::allocator<void>{});
                };
                app->postEvent(workQueue, new WorkUnitEvent(work));
                return lua_yield(L, 0);
            })
    EMILUA_GPERF_END(key)(L, vm_ctx, current_fiber, context);
}

static int qml_context_property_mt_index(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& context = *static_cast<std::shared_ptr<QQmlContext>*>(
        lua_touserdata(L, 1));
    auto key = tostringview(L, 2);

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,context,
        key=QString::fromUtf8(key.data(), key.size())
    ]() {
        auto var = context->contextProperty(key);
        vm_ctx->strand().post([vm_ctx,current_fiber,var,context]() {
            on_index_resume_fiber(*vm_ctx, current_fiber, var, context);
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern "C" BOOST_SYMBOL_EXPORT std::error_code init_lua_module(lua_State* L);
#pragma clang diagnostic pop

std::error_code init_lua_module(lua_State* L)
{
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "load_qml");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, load_qml);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "register_resource");
        lua_pushcfunction(L, register_resource);
        lua_rawset(L, -3);

        lua_pushliteral(L, "unregister_resource");
        lua_pushcfunction(L, unregister_resource);
        lua_rawset(L, -3);
    }

    lua_pushlightuserdata(L, &qml_context_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.qml_context");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        lua_pushcfunction(L, qml_context_mt_index);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<std::shared_ptr<QQmlContext>>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    lua_pushlightuserdata(L, &qml_context_property_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.qml_context.property");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, qml_context_property_mt_index);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<std::shared_ptr<QQmlContext>>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    lua_pushlightuserdata(L, &qobject_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/4);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.object");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, qobject_mt_index);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__newindex");
        lua_pushcfunction(L, qobject_mt_newindex);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__call");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, qobject_mt_call);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<std::shared_ptr<QObject>>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    return {};
}

} // namespace emilua_qt
