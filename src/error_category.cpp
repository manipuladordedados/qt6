/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua_qt/error_category.hpp>

namespace emilua_qt {

class category_impl: public std::error_category
{
public:
    const char* name() const noexcept override;
    std::string message(int value) const noexcept override;
};

const char* category_impl::name() const noexcept
{
    return "emilua_qt";
}

std::string category_impl::message(int value) const noexcept
{
    switch (value) {
    case static_cast<int>(errc::failed_to_load_qml):
        return "Failed to load QML";
    case static_cast<int>(errc::wrong_signature):
        return "Signature doesn't match";
    case static_cast<int>(errc::too_many_arguments):
        return "Q_METAMETHOD_INVOKE_MAX_ARGS reached";
    default:
        return {};
    }
}

const std::error_category& category()
{
    static category_impl cat;
    return cat;
}

} // namespace emilua_qt
