/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua/core.hpp>
#include <emilua_qt/qt_handle.hpp>
#include <future>
#include <boost/dll.hpp>

namespace emilua_qt {

namespace dll = boost::dll;

qt_handle::qt_handle()
    : init_lua_module{nullptr}
    , loop_end{nullptr}
{
    std::promise<std::tuple<
        /*init_lua_module=*/std::error_code (*)(lua_State*),
        /*loop_end=*/void (*)()
    >> p;
    auto f = p.get_future();

    thread = std::thread{[p=std::move(p)]() mutable {
        auto lib = std::make_unique<dll::shared_library>();

        try {
            lib->load(dll::this_line_location().parent_path() / "qt6_loop",
                      dll::load_mode::append_decorations);
        } catch (const std::system_error&) {
            p.set_exception(std::current_exception());
            return;
        }

        lib->get<void()>("init_loop")();

        p.set_value({
            lib->get<std::error_code(lua_State*)>("init_lua_module"),
            lib->get<void()>("loop_end")
        });

        lib->get<void()>("loop_start")();

        // Once execution starts, Qt can no longer be unloaded. Therefore WE
        // INTENTIONALLY LEAK DATA HERE to retain Qt program text in mapped
        // memory.
        //
        // Qt will do stuff such as:
        //
        // * Start threads that are never joined (e.g. QDBusConnection).
        // * Register thread-local storage to threads other than the
        //   QCoreApplication thread (e.g. any thread calling
        //   QThread::currentThread()).
        //
        // These are just a few examples, but the net effect is that we cannot
        // unload Qt code as that would cause the process to jump into unmapped
        // memory.
        lib.release();
    }};

    try {
        std::tie(
            const_cast<std::error_code (*&)(lua_State*)>(init_lua_module),
            const_cast<void (*&)()>(loop_end)
        ) = f.get();
    } catch (...) {
        thread.join();
        throw;
    }
}

qt_handle::~qt_handle()
{
    loop_end();
    thread.join();
}

} // namespace emilua_qt
