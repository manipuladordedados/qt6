/* Copyright (c) 2023 Vinícius dos Santos Oliveira

   Distributed under the Boost Software License, Version 1.0. (See accompanying
   file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt) */

#include <emilua_qt/observer.hpp>
#include <QtCore/QVariant>

namespace emilua_qt {

using emilua::vm_context;
using emilua::push;

Observer::Observer(std::shared_ptr<vm_context> vm_ctx, int function,
                   QObject *parent)
    : QObject{parent}
    , vm_ctx{std::move(vm_ctx)}
    , function{function}
    , work_guard{this->vm_ctx->work_guard()}
{}

Observer::~Observer()
{
    vm_ctx->strand().post([vm_ctx=this->vm_ctx,function=this->function]() {
        if (!vm_ctx->valid())
            return;

        lua_State* L = vm_ctx->async_event_thread();
        luaL_unref(L, LUA_REGISTRYINDEX, function);
    }, std::allocator<void>{});
}

void Observer::map()
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            int res = lua_pcall(L, 0, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::map(bool b)
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal,b
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            lua_pushboolean(L, b);
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::map(double d)
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal,d
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            lua_pushnumber(L, d);
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::map(int i)
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal,i
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            lua_pushinteger(L, i);
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::map(const QString& s)
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal,s=s.toUtf8()
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            try {
                lua_pushlstring(L, s.data(), s.size());
            } catch (...) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::map(const QUrl& u)
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal,u=u.toEncoded()
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            try {
                lua_pushlstring(L, u.data(), u.size());
            } catch (...) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::map(const QVariant& v)
{
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            signal=this->subscribedSignal,v
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            try {
                switch (v.typeId()) {
                case QMetaType::Bool:
                    lua_pushboolean(L, v.toBool());
                    break;
                case QMetaType::Double:
                    lua_pushnumber(L, v.toDouble());
                    break;
                case QMetaType::Int:
                    lua_pushinteger(L, v.toInt());
                    break;
                case QMetaType::QString:
                case QMetaType::QUrl: {
                    auto s = v.toString().toUtf8();
                    lua_pushlstring(L, s.data(), s.size());
                    break;
                }
                default:
                    lua_pushnil(L);
                }
            } catch (...) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("%s callback failed: %s", signal.c_str(),
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::onEngineExit(int retCode)
{
    vm_ctx->strand().post(
        [vm_ctx=this->vm_ctx,function=this->function,retCode]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            lua_pushinteger(L, retCode);
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("onExit() callback failed: %s", lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::onEngineQuit()
{
    vm_ctx->strand().post(
        [vm_ctx=this->vm_ctx,function=this->function]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            int res = lua_pcall(L, 0, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("onQuit() callback failed: %s", lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

void Observer::onEngineWarnings(const QList<QQmlError> &warnings)
{
    std::vector<std::string> warnings2;
    warnings2.reserve(warnings.size());
    for (const auto& w : warnings) {
        warnings2.emplace_back(w.toString().toStdString());
    }
    vm_ctx->strand().post(
        [
            vm_ctx=this->vm_ctx,function=this->function,
            warnings=std::move(warnings2)
        ]() mutable {
            if (!vm_ctx->valid())
                return;

            lua_State* L = vm_ctx->async_event_thread();
            lua_rawgeti(L, LUA_REGISTRYINDEX, function);
            try {
                lua_createtable(L, /*narr=*/0, /*nrec=*/warnings.size());

                for (int i = 0 ; i != warnings.size() ; ++i) {
                    push(L, warnings[i]);
                    lua_rawseti(L, -2, i + 1);
                }
            } catch (...) {
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            }
            int res = lua_pcall(L, 1, 0, 0);
            switch (res) {
            case 0:
                break;
            case LUA_ERRMEM:
                vm_ctx->notify_errmem();
                vm_ctx->close();
                return;
            case LUA_ERRRUN:
                qWarning("onWarnings() callback failed: %s",
                         lua_tostring(L, -1));
                lua_settop(L, 0);
                break;
            }
        }, std::allocator<void>{});
}

} // namespace emilua_qt
